const path = require('path')
module.exports = {
  devServer: {
    port: 9527,
    open: true,
    proxy: {
      '/api': {
        target: 'http://rap2api.taobao.org/app/mock/275133',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api'
        }
      }
    }
  },
  lintOnSave: false,
  chainWebpack: config => {
    config.resolve.alias
      .set('@', path.join(__dirname, 'src'))
      .set('assets', path.join(__dirname, 'src/assets'))
      .set('components', path.join(__dirname, 'src/components'))
      .set('views', path.join(__dirname, 'src/views'))
      .set('utils', path.join(__dirname, 'src/utils'))
      .set('api', path.join(__dirname, 'src/api'))
      .set('mixins', path.join(__dirname, 'src/mixins'))
  }/* ,
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-pxtorem')({ // 把px单位换算成rem单位
            rootValue: 75, // 换算的基数(设计图750的根字体为75)
            propList: ['width', 'height', 'padding', 'margin']//* 代表将项目中的全部进行转换，单个转换如width、height等
          })
        ]
      }
    }
  } */
}
