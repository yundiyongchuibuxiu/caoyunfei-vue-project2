import request from '../utils/request.js'

// 文章列表接口
const fetchArtLists = (params = {}) => request.get('/api/v1/artList', { params })

// 添加文章接口
const fetchArtAdd = (params) => request.post('/api/v1/addArt', params)

// 获取文章详情接口
const getArtById = (id) => request.get('/api/v1/getArt', {
  params: {
    id
  }
})

// 跟新文章的接口
const updataArt = (params) => request.post('/api/v1/uploadArt', params)

// 删除文章接口
const delArt = (id) => request.get('/api/v1/delArt', {
  params: {
    id
  }
})

// 登录接口
const doLogin = (params) => request.post('/api/v1/login', params)

export {
  fetchArtLists,
  fetchArtAdd,
  getArtById,
  updataArt,
  delArt,
  doLogin
}
