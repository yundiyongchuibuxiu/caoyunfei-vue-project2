// 截取字符串
const splitStr = (str, n) => str.substr(0, n) + '...'

// 判断是否登录
const isLogin = () => !!localStorage.getItem('token')
// 判断token是否存在，如果不存在就是 undefinde,取反会隐式转换成true，再取反就是false
// 如果存在，就是字符串，取反隐式转换为false，再取反就是true

export {
  splitStr,
  isLogin
}
