import axios from 'axios'
// import Vue from 'vue'
import qs from 'qs'
import { message } from 'ant-design-vue'
// const vm = new Vue()
const request = axios.create({
  baseURL: '',
  timeout: 8000
})

// 请求拦截
request.interceptors.request.use(function (config) {
  // vm.$showLoading()
  // 请求头中添加token
  const token = localStorage.getItem('token')
  if (token) {
    config.headers.access_token = token
  }
  // 解决 post请求，参数，content-type 为 application/json 的问题
  if (config.method === 'post' && config.data) {
    config.data = qs.stringify(config.data)
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// 响应拦截
request.interceptors.response.use(function (res) {
  // vm.$hideLoading()
  // 判断token是否过期，或者是不是没有传token
  // 设置token没有传就是401，过期就是402
  if (res.data.code === 401 || res.data.code === 402) {
    // 如果没有传token或者token过期，则弹出全局提示
    message.error('token丢失，或者过期', 1.5, () => {
      // 然后重定向到登录页
      this.$router.push('/login')
      // history.go(0)
    })
  }
  return res
}, function (error) {
  return Promise.reject(error)
})

export default request
