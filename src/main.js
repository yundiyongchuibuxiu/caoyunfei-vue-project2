import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { Button, Layout, Menu, Icon, Breadcrumb, Card, Table, Tooltip, Avatar, Tag, ConfigProvider, Row, Col, Form, Input, Upload, Dropdown, Badge, List } from 'ant-design-vue'

Vue.config.productionTip = false
Vue.use(Layout)
  .use(Menu)
  .use(Icon)
  .use(Breadcrumb)
  .use(Button)
  .use(Card)
  .use(Table)
  .use(Tooltip)
  .use(Avatar)
  .use(Tag)
  .use(ConfigProvider)
  .use(Row)
  .use(Col)
  .use(Form)
  .use(Input)
  .use(Upload)
  .use(Dropdown)
  .use(Badge)
  .use(List)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
