import Vue from 'vue'
import Vuex from 'vuex'
import usere from './user/index.js'
import msge from './msg/index.js'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    usere,
    msge
  },
  plugins: [
    createPersistedState()
  ]
})
