export default {
  set_msgLists (state, index) {
    state.msgLists[index].isCompleted = !state.msgLists[index].isCompleted
  },
  del_todo (state, index) {
    state.msgLists.splice(index, 1)
  },
  del_msgLists (state, _msge) {
    // 退出登录时删除vuex里面的msgLists
    state.msgLists = _msge
  }
}
