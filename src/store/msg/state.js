export default {

  msgLists: [
    {
      context: '一去二三里',
      isCompleted: false
    },
    {
      context: '烟村四五家',
      isCompleted: false
    },
    {
      context: '亭台六七座',
      isCompleted: false
    },
    {
      context: '八九十支花',
      isCompleted: true
    }
  ]
}
