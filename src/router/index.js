import Vue from 'vue'
import VueRouter from 'vue-router'
import DashBoard from '../views/DashBoard'
import { isLogin } from 'utils'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/admin/dashBoard',
    name: '/',
    roles: ['admin', 'superAdmin', 'aditor']
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('views/Admin'),
    meta: {
      needLogin: true,
      keepAlive: true,
      isNav: true,
      roles: ['admin', 'superAdmin', 'aditor']
    },
    children: [
      {
        path: '/admin/dashBoard',
        name: '仪表盘',
        component: DashBoard,
        icon: 'area-chart',
        meta: {
          needLogin: true,
          keepAlive: true,
          isNav: true,
          roles: ['admin', 'superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/artLists',
        name: '文章管理',
        component: () => import('views/ArtLists'),
        icon: 'unordered-list',
        meta: {
          needLogin: true,
          keepAlive: true,
          isNav: true,
          roles: ['admin', 'superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/artEdit/:artId',
        name: '编辑文章',
        component: () => import('views/ArtEdit'),
        meta: {
          isNav: false,
          needLogin: true,
          keepAlive: true,
          roles: ['admin', 'superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/artAdd',
        name: '增加文章',
        component: () => import('views/ArtAdd'),
        meta: {
          isNav: false,
          needLogin: true,
          keepAlive: true,
          roles: ['admin', 'superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/msgLists',
        name: '消息中心',
        component: () => import('views/MsgLists'),
        icon: 'message',
        meta: {
          needLogin: true,
          keepAlive: true,
          isNav: true,
          roles: ['admin', 'superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/settings',
        name: '设置',
        component: () => import('views/Settings'),
        icon: 'setting',
        meta: {
          needLogin: true,
          keepAlive: true,
          isNav: true,
          roles: ['superAdmin', 'aditor']
        }
      },
      {
        path: '/admin/noPermission',
        name: '没有权限',
        component: () => import('views/NoPermission'),
        meta: {
          needLogin: true,
          keepAlive: false,
          isNav: false,
          roles: ' * '
        }
      }
    ]
  },
  {
    path: '/login',
    name: '登录',
    component: () => import('views/Login'),
    meta: {
      needLogin: false,
      keepAlive: false,
      isNav: false,
      roles: ['admin', 'superAdmin', 'aditor']
    }
  },
  {
    path: '/admin/404',
    name: '404',
    component: () => import('views/NotFound'),
    meta: {
      isNav: false,
      needLogin: false,
      roles: ['admin', 'superAdmin', 'aditor']
    }
  },
  {
    path: '*',
    name: '*',
    component: () => import('views/NotFound'),
    meta: {
      isNav: false,
      needLogin: false,
      roles: ['admin', 'superAdmin', 'aditor']
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

// 路由前置守卫
router.beforeEach((to, from, next) => {
  routes.map(route => {
    if (to.meta.roles === '*') {
      // 如果是*，那么就是可以访问
      next()
    }
    // const role = localStorage.getItem('role')
    // console.log(role)
    // role是当前访问路由的角色
    const roles = to.meta.roles
    // roles 是路由规则中定义的路由角色
    const hasPermission = roles.includes(localStorage.getItem('role'))
    if (hasPermission) {
      // 可以访问
      next()
    } else {
      // 无法访问
      next({
        // path: '/admin/noPermission'
      })
    }
  })

  //  判断目标路由是否需要登录权限
  if (to.meta.needLogin) {
    // 先判断是否登录
    if (isLogin()) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          from: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

export default router
